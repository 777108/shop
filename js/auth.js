const form = document.getElementById('form');
const submit = document.getElementById('register');

submit.addEventListener('click', saveUser);

const BASE_URL = 'https://shop-itclass.herokuapp.com';

const token = localStorage.getItem('token');
if (token) {
    closeModal();
}

function saveUser() {
    const formData = new FormData(form);
    const emailPattern = /^([a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$)/gmi;
    const emailIsValid = emailPattern.test(formData.get('email'));
    const password = formData.get('password');
    const confirm = formData.get('confirm');
    const passwordIsValid = (password === confirm) && (password.length >= 5);
    if (emailIsValid && passwordIsValid) {
        const user = {
            email: formData.get('email'),
            password: formData.get('password')
        };
        registerUser(user).then(data => {
            data.json().then(result => { 
                localStorage.setItem('token', result);
                closeModal();
            });
        });
    }
}

function closeModal() {
    document.getElementsByClassName('wrapper')[0].style.display = 'none';
}

function registerUser(data) {
    return fetch(BASE_URL.concat('/register'), {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
}

document.getElementById('getData').addEventListener('click', getUserData);

function getUserData() {
    fetch(BASE_URL.concat('/user', '?id=', token), {
        method: 'GET'
    }).then(result => result.json().then(data => console.log(data)));
}
